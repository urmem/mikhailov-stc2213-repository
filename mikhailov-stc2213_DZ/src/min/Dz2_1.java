package min;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;


/*Условие:
Есть, неопределенная в количестве (т.е. чисел может быть и 5, и 100, и 500 и т.д.), последовательность чисел:
 а0, а1, а2 ... аN
 N -> бесконечность, aN -> -1 (последнее число в последовательности всегда -1)
 2.1 Нужно написать псевдокод алгоритма нахождения минимального числа среди всей последовательности.
 Под конец алгоритма вывести минимальное число.
 2.2 Нужно написать псевдокод алгоритма, который посчитает количество всех четных чисел и нечетных в
 последовательности. Под конец алгоритма вывести количество четных и нечетных чисел.
 Подсказка: для выполнения данной задачи нужно воспользоваться оператором %
*/
public class Dz2_1 {

    public static void main(String[] args) {


        int x;
        int y = 0;

        ArrayList<Integer> spisok = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        x = sc.nextInt();
        try {
            while (x != -1) {
                spisok.add(x);
                x = sc.nextInt();
            }
            int min = spisok.get(0);
            for (int i = 0; i < spisok.size(); i++) {
                if (min > spisok.get(i)) {
                    min = spisok.get(i);
                }


            }
            System.out.println();
            System.out.print("Из всех введенных чисел самым минимальным является " + min);

        } catch (Exception e) {
            System.out.println("Вы не ввели число");
        }
    }
}



